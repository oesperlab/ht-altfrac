'''
This program will compute the H/T alternating fraction for a set of supplied
rearrangment adjacencies.

By Layla Oesper
'''
import argparse
import sys
from data_tools import *
import os

def parse_arguments():
    '''
    Parse command line arguments.

    Returns:
        data_dir (string): directory containing input data files
        data_list (string): filename of file located in data_dir that contains
                            a list of all data files to analyze.
        out_file (string): the file to create with the results.
        suffix (string): suffix for filenames
    '''

    parser = argparse.ArgumentParser()
    parser.add_argument("-d","--DATA_DIR", help="Directory where the data \
        files are located", metavar="DATA_DIR", required=True)
    parser.add_argument("-l","--DATA_LIST", help="File containing list of \
        names of files in the data directory",metavar="DATA_LIST", required=True)
    parser.add_argument("-o","--OUT_FILE", help="The name of the filed to \
        create with the output.", default="./AllResults.txt",metavar="OUT_FILE",\
        required=False)
    parser.add_argument("-s","--SUFFIX", help="Suffix for all data file names", \
        default="", metavar="SUFFIX", required=False)

    args = parser.parse_args()

    data_dir = args.DATA_DIR
    data_list = args.DATA_LIST
    out_file = args.OUT_FILE
    suffix = args.SUFFIX

    print("=================================================")
    print("Arguments are:")
    print("\tData Directory: %s" % data_dir)
    print("\tData List: %s" % data_list)
    print("\tOutput File: %s" % out_file)
    print("\tData File Suffix: %s" % suffix)

    return data_dir, data_list, out_file, suffix

def compute_AF(data_dir, data_list, suffix):
    '''Reads in all data files from data_list (files located in data_dir)
    and computes the AF value for each.

    Input:
        data_dir (string) - directory containing input data files.
        data_list (string) - file containing names of each input file.
        suffix (string) - suffix for all data files

    Returns:
        results (list of lists) - one entry in the list for each sample.  Each
                                sample is a list of values: ID, AF, other values
                                included in the input file.
    '''
    samples = load_data_list(data_list)

    #Iterate through samples and compute value.
    results = []

    for sample in samples:
        sample_file_name = sample[0] + suffix
        sample_file_path = os.path.join(data_dir,sample_file_name)
        if os.path.isfile(sample_file_path):
            sample_AF = get_sample_results(sample_file_path, sample_file_name)
            sample_results = [sample_file_name, sample_AF]
            results.append(sample_results)
        else:
            print("Unable to find file: %s" % sample_file_path)
            print("Skipping and moving on to next sample.\n")

    return results

def get_sample_results(sample_file_path, sample_file_name):
    '''
    Reads in data from a sample file and returns the associated AF value.

    Input:
        sample_file_path (string) - the full path the sample file to analyze.
        sample_file_name (string) - just the name of the sample file.  used
        as an ID in the CS_Data class.

    Return:
        AF (float) - the computed H/T alternating fraction value for the sample.
    '''
    cs_obj = CS_Data(sample_file_name, sample_file_path)
    AF = cs_obj.frac_alt_pairs()

    return AF

def save_results(results, out_file):
    '''
    Save the compute results to file.

    Input:
        results (list of lists) - results computed for all samples.
        out_file (string) - the file in which to save the computed results.
    '''

    file = open(out_file, 'w')
    file.write("#Sample\tAF(C)\n")
    for line in results:
        vals = [str(x) for x in line]
        sample_str = '\t'.join(vals) + '\n'
        file.write(sample_str)

    file.close()


def load_data_list(data_list):
    '''Reads in data from data_list.

    Input:
        data_list (string) - list of data files to analyze.

    Returns:
        data (list of tuples) - list of tuples of data ID and other information
        included in input file.
    '''
    samples = []
    try:
        file = open(data_list, 'r')
        for line in file:
            if line.startswith("#"): continue #skip header line
            vals = line.rstrip().split('\t')
            samples.append(tuple(vals))
    except:
        print("\nProblem with file: %s" % data_list)
        print("Try again.  Exiting program.")
        sys.exit(-1)

    return samples


def main():

    #Retrieve command line parameters
    data_dir, data_list, out_file, suffix = parse_arguments()

    #Read in data and compute AF values over each.
    results = compute_AF(data_dir, data_list, suffix)

    #Save results to file
    save_results(results, out_file)



if __name__ == '__main__':
    main()
