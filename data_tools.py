'''
This module contains code to help with chromothripsis
real data analysis.
'''

import random
import sys
random.seed(3742674)

class CS_Data:

    def __init__(self, name, filename):
        '''
        Create an ojbect that contains information about a single dataset.
        '''

        #Basic info
        self.name = name
        self.filename = filename

        #Open file and create adjacency list
        adjacencies = []
        try:
            file = open(filename, 'r')
            data = file.readlines()
            data = data[1:] #remove header

            for line in data:
                vals = line.rstrip().split('\t')
                bp1 = (int(vals[0]), int(vals[1]), int(vals[2]))
                bp2 = (int(vals[3]), int(vals[4]), int(vals[5]))
                adjacencies.append([bp1, bp2])

            self.adjacencies = adjacencies

            self.bps = self.compute_bps()
            #print(self.bps)
            file.close()
        except:
            print("Error!  Problem with file: %s" % filename)
            print("Exiting program.")
            sys.exit(-1)

    def get_bps(self):
        return self.bps

    def get_adjacencies(self):
        return self.adjacencies

    def get_num_bps(self):
        return 2 * len(self.adjacencies)

    def get_num_bps_most_chrm(self):
        chrm = self.chrm_most_bps()
        return len(self.bps[chrm])

    def get_num_chrms(self):
        return len(self.bps)

    def compute_bps(self):
        '''
        Create a dictionary by chromosome that is ordered
        by genomic position.
        '''
        bps = {}
        for adj in self.adjacencies:
            for bp in adj:

                chrm = bp[0]
                pos = bp[1]
                std = bp[2]

                if chrm not in bps:
                    bps[chrm] = [(pos, std)]
                else:
                    bps[chrm].append((pos,std))

        #Sort according to genomic position
        for chrm in bps:
            bps[chrm] = sorted(bps[chrm])

        return bps

    def frac_chrms_alternate(self):
        '''
        Returns the fraction of chromosomes, with at least
        one bp, that have an alternating pattern.
        '''
        bps = self.get_bps()
        num_chrms = len(bps)
        num_alt = 0

        for chrm in bps:
            pos = bps[chrm]
            if is_alternating(pos):
                num_alt += 1

        return float(num_alt) / float(num_chrms)

    def chrm_most_bps(self):
        '''
        Returns the chromosome with the most number of bps.
        '''
        bps = self.get_bps()

        most = None
        num = 0
        for chrm in bps:
            cur = len(bps[chrm])
            if cur > num:
                num = cur
                most = chrm

        return most

    def chrm_most_alt(self):
        '''
        Returns true if the chrm with the most
        bps alternates, otherwise false.
        '''
        most = self.chrm_most_bps()
        pos = self.get_bps()[most]
        alt = is_alternating(pos)
        return alt

    def frac_alt_pairs(self):
        '''
        Returns the fraction of adjacent bps across
        the whole sample that alternate.
        '''
        bps = self.get_bps()
        alt = 0
        poss = 0

        for chrm in bps:
            pos = bps[chrm]
            chrm_poss, chrm_alt = get_alt_numbers(pos)

            alt += chrm_alt
            poss += chrm_poss

        return float(alt) / float(poss)

    def chrm_frac_alt(self):
        '''
        Returns the fractions of BP pairs in the chrm with the most bps that alternate.
        '''
        most = self.chrm_most_bps()
        pos = self.get_bps()[most]
        poss, alt = get_alt_numbers(pos)
        return float(alt) / float(poss)

    def randomize(self):
        '''
        Will take the CS_Data object and randomize
        the observed breakpoints as H or T.  To be
        use in conjunction with simulations based
        on real data.
        '''

        ##FOR NOW ONLY RANDOMIZES BPS NOT ADJS!!##
        bps = self.get_bps()
        for chrm in bps:
            list_bps = bps[chrm]
            for idx in range(len(list_bps)):
                cur_tup = list_bps[idx]
                std = random.randint(0,1)
                pos = cur_tup[0]
                new_tup = (pos, std)
                list_bps[idx] = new_tup
        self.bps = bps

    ## End CS_Data Class


def get_alt_numbers(pos):
    '''
    Returns the number of possible adjacent pairs of bps
    and the number that alternate in the given list of
    positions.
    '''
    tot = len(pos)
    chrm_poss = tot - 1 # there are one less adjacent pairs
    chrm_alt = 0

    last = pos[0][1]
    for idx in range(1, len(pos)):
        cur = pos[idx][1]
        if cur != last:
            chrm_alt += 1
        last = cur

    return chrm_poss, chrm_alt

def is_alternating(pos):
    '''
    Returns True if the submitted list of bps is
    alternating and False otherwise

    pos: list of tuples of (pos, std)
    '''

    if len(pos) == 0:
        return False

    last = pos[0][1]
    for idx in range(1, len(pos)):
        cur = pos[idx][1]
        if cur == last:
            return False
        else:
            last = cur

    return True

def main():
    filename = '../AllChains/OV-9T-chainID-1.csv.forMatlab'
    ID = 'OV-9T-chainID-1'

    cs_obj = CS_Data(ID, filename)
    print(len(cs_obj.adjacencies))
    print(len(cs_obj.bps))

    alt_frac = cs_obj.frac_chrms_alternate()
    print('Fraction alternating: ', alt_frac)

    chrm_most = cs_obj.chrm_most_bps()
    print('Chrm with most: ', chrm_most)

    chrm_most_alt = cs_obj.chrm_most_alt()
    print('Chrm with most alt?', chrm_most_alt)

    frac_alt_pairs = cs_obj.frac_alt_pairs()
    print('Fraction alt bps: ', frac_alt_pairs)

    chrm_frac_alt = cs_obj.chrm_frac_alt()
    print('Frac alt in chrm with most: ', chrm_frac_alt)

    print('Original BPs.')
    print(cs_obj.get_bps())
    cs_obj.randomize()
    print('Randomized BPs.')
    print(cs_obj.get_bps())

if __name__ == '__main__':
    main()
